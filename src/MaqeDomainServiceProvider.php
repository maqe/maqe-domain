<?php

namespace Maqe\MaqeDomain;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class MaqeDomainServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public function boot()
    {
        $this->configurePublishing();
        $this->configureCommands();
        $this->resolveFactoryDomainName();
    }

    protected function configurePublishing()
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->publishes([
            __DIR__ . '/../config/maqe-domain.php' => config_path('maqe-domain.php'),
        ], 'maqe-domain-config');
    }

    protected function configureCommands(): void
    {
        if (! $this->app->runningInConsole()) {
            return;
        }

        $this->commands([
            Console\InstallCommand::class,
            Console\CreateBaseCommand::class,
            Console\CreateApiCommand::class,
        ]);
    }

    private function resolveFactoryDomainName()
    {
        Factory::guessFactoryNamesUsing(function (string $modelName) {
            $namespace = 'Database\\Factories\\';
            $appNamespace = 'App\\';
            $domainNamespace = $appNamespace . 'Domain\\';

            $guessNamespace = '';

            if (Str::startsWith($modelName, $domainNamespace)) {
                $guessNamespace = 'Domain\\' . Str::between($modelName, $domainNamespace, 'Models\\');
            }

            $modelName = Str::afterLast($modelName, '\\');

            return $namespace . $guessNamespace . $modelName . 'Factory';
        });
    }
}
